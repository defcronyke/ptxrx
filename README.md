# ptxrx - in early development (not ready to be used yet)

## Documentation and references

- Main repo:

  - [https://gitlab.com/defcronyke/ptxrx](https://gitlab.com/defcronyke/ptxrx)

- Workspace repo:
  - [https://gitlab.com/defcronyke/ptxrx-workspace](https://gitlab.com/defcronyke/ptxrx-workspace)

## Usage

coming soon

## Development quickstart

1. (a bunch of `git clone`ing and symlinking. It will be explained in the coming days as I work out the best methods for it)
2. `cd ptxrx-workspace`
3. work on the code in this folder and commit each `git` repo individually
4. build the workspace with the following command:

   ```rust
   cargo build
   ```

5. start `dragonflydb` by running the following `bash` script, like this:

   ```shell
   cd ..
   ./run_dragonflydb.sh
   ```

6. run any of the `rust` workspace "\_bin" projects in a separate terminal, using `cargo run -p ...`, like this:

   ```rust
   cargo run -p ptxrx_redis_bin
   ```

### Resources

1. [https://git-scm.com/book/en/v2/Git-Tools-Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)
2. [https://doc.rust-lang.org/book/ch14-03-cargo-workspaces.html](https://doc.rust-lang.org/book/ch14-03-cargo-workspaces.html)
