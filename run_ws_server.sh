#!/bin/sh
if [ -d "./ptxrx-workspace" ]; then
    cd ptxrx-workspace
elif [ -d "./ptxrx" ]; then
    cd ptxrx
fi
cargo run -p ptxrx_websocket_server_bin