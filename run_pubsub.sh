#!/bin/sh
if [ -d "./ptxrx-workspace" ]; then
    cd ptxrx-workspace
elif [ -d "./ptxrx" ]; then
    cd ptxrx
fi
cargo run -p ptxrx_redis_pubsub_bin