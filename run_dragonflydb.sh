#!/bin/bash

if [ -d "./ptxrx-workspace" ]; then
    cd ptxrx-workspace
elif [ -d "./ptxrx" ]; then
    cd ptxrx
fi

# Binary version (tested on Arch Linux)
if [ ! -f "./bin/dragonfly-x86_64.tar.gz" ]; then
    mkdir bin
    cd bin
    wget https://dragonflydb.gateway.scarf.sh/latest/dragonfly-x86_64.tar.gz
    tar zxvf dragonfly-x86_64.tar.gz
    cd ..
    # TODO: Checksum of binary here
fi

# remove previous db dump files
rm ./dump-*.dfs

bin/dragonfly-x86_64 --version
bin/dragonfly-x86_64 --help
bin/dragonfly-x86_64

# Docker version (tested on Arch Linux. not ideal.)
#docker run --network=host --ulimit memlock=-1 -p 6379:6379 --name ptxrx-dragonflydb1 docker.dragonflydb.io/dragonflydb/dragonfly
